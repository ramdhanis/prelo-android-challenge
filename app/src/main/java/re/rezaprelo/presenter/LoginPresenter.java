package re.rezaprelo.presenter;

import android.util.Log;

import com.google.gson.Gson;

import javax.inject.Inject;

import re.rezaprelo.App;
import re.rezaprelo.helper.ErrorRetrofitUtil;
import re.rezaprelo.model.LoginRequest;
import re.rezaprelo.model.LoginResponse;
import re.rezaprelo.presenter.mvpview.LoginMvpView;
import re.rezaprelo.services.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Reza on 14/12/2017.
 */

public class LoginPresenter  implements BasePresenter<LoginMvpView>{


    /**
     * Injection API Service
     */
    @Inject
    ApiService mApiService;

    /**
     * Injection error response from retrofit
     */
    @Inject
    ErrorRetrofitUtil mErrorRetrofitUtil;

    private LoginMvpView loginMvpView;
    private Call<LoginResponse> call;
    private LoginResponse loginResponse;

    public LoginPresenter() {
        App.appComponent().inject(this);
    }


    @Override
    public void attachView(LoginMvpView view) {

        loginMvpView = view;
    }

    @Override
    public void detachView() {
        loginMvpView = null;
        if (call != null) call.cancel();
    }

    public void callLogin(LoginRequest loginRequest) {
        loginMvpView.onPreLogin();
        call = mApiService.login(loginRequest);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response != null) {
                    if (response.code() == 200) {
                        processData(response);
                        loginMvpView.onSuccessLogin(response.body());
                    } else {
                        loginMvpView.onFailedLogin(response.message());
                    }
                } else {
                    loginMvpView.onFailedLogin(mErrorRetrofitUtil.parseError(response)
                            .getMeta().getMessage());
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                loginMvpView.onFailedLogin(mErrorRetrofitUtil.responseFailedError(t));
            }
        });
    }

    /**
     * Processing data from retrofit for save in preference
     * @param response response data from retrofit
     */
    private void processData(Response<LoginResponse>response) {
        loginResponse = response.body();
        Log.d("DATA MODEL", String.valueOf(response));
    }

}


