package re.rezaprelo.presenter;

import javax.inject.Inject;

import re.rezaprelo.helper.ErrorRetrofitUtil;
import re.rezaprelo.model.LoginResponse;
import re.rezaprelo.presenter.mvpview.AttachmentMvpView;
import re.rezaprelo.services.ApiService;
import retrofit2.Call;

/**
 * Created by Reza on 14/12/2017.
 */

public class AttachmentPresenter  implements BasePresenter<AttachmentMvpView> {
    @Inject
    ApiService apiService;

    @Inject
    ErrorRetrofitUtil errorRetrofitUtil;

    private AttachmentMvpView mAttachmentMvpView;
    private Call<LoginResponse> call;

    public AttachmentPresenter() {
        //App.appComponent().inject(this);
    }

    @Override
    public void attachView(AttachmentMvpView view) {
        mAttachmentMvpView = view;
    }

    @Override
    public void detachView() {
        mAttachmentMvpView = null;
        if (call != null) call.cancel();
    }
}

