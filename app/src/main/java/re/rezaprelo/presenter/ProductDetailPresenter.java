package re.rezaprelo.presenter;

import android.util.Log;

import javax.inject.Inject;

import re.rezaprelo.App;
import re.rezaprelo.helper.ErrorRetrofitUtil;
import re.rezaprelo.model.LoveListResponse;
import re.rezaprelo.model.detailProduct.ProductDetailResponse;
import re.rezaprelo.presenter.mvpview.DetailProductMvpView;
import re.rezaprelo.presenter.mvpview.LoveListMvpView;
import re.rezaprelo.services.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Reza on 27/12/2017.
 */

public class ProductDetailPresenter implements BasePresenter<DetailProductMvpView> {


    /**
     * Injection API Service
     */
    @Inject
    ApiService mApiService;

    /**
     * Injection error response from retrofit
     */
    @Inject
    ErrorRetrofitUtil mErrorRetrofitUtil;

    private DetailProductMvpView loveListMvpView;
    private Call<ProductDetailResponse> call;

    public ProductDetailPresenter() {
        App.appComponent().inject(this);
    }


    @Override
    public void attachView(DetailProductMvpView view) {

        loveListMvpView = view;
    }

    @Override
    public void detachView() {
        loveListMvpView = null;
        if (call != null) call.cancel();
    }

    public void callProductDetail(String productId) {
        loveListMvpView.onPreLoading();
        call = mApiService.productDetail(productId);
        call.enqueue(new Callback<ProductDetailResponse>() {
            @Override
            public void onResponse(Call<ProductDetailResponse> call, Response<ProductDetailResponse> response) {
                if (response != null) {
                    if (response.code() == 200) {
                        processData(response);
                        loveListMvpView.onSuccessProductDetail(response.body());
                    } else {
                        loveListMvpView.onFailedProductDetail(response.message());
                    }
                } else {
                    loveListMvpView.onFailedProductDetail(mErrorRetrofitUtil.parseError(response)
                            .getMeta().getMessage());
                }
            }

            @Override
            public void onFailure(Call<ProductDetailResponse> call, Throwable t) {
                loveListMvpView.onFailedProductDetail(mErrorRetrofitUtil.responseFailedError(t));
            }
        });
    }

    /**
     * Processing data from retrofit for save in preference
     *
     * @param response response data from retrofit
     */
    private void processData(Response<ProductDetailResponse> response) {
        Log.d("DATA MODEL", String.valueOf(response));
    }

}


