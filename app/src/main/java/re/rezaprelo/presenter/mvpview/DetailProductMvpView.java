package re.rezaprelo.presenter.mvpview;

import re.rezaprelo.model.LoveListResponse;
import re.rezaprelo.model.detailProduct.ProductDetailResponse;

/**
 * Created by Reza on 27/12/2017.
 */

public interface DetailProductMvpView extends BaseMvpView {

    void onPreLoading();

    void onSuccessProductDetail(ProductDetailResponse productDetailResponse);

    void onFailedProductDetail(String message);
}
