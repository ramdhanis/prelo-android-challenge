package re.rezaprelo.presenter.mvpview;

import re.rezaprelo.model.LoginResponse;

/**
 * Created by Reza on 14/12/2017.
 */

public interface LoginMvpView extends BaseMvpView {
    void onPreLogin();
    void onSuccessLogin(LoginResponse loginResponse);
    void onFailedLogin(String message);
}
