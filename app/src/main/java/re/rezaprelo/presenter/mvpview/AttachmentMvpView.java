package re.rezaprelo.presenter.mvpview;

/**
 * Created by Reza on 14/12/2017.
 */

public interface AttachmentMvpView extends BaseMvpView {
    void onPreSubmitAttachment();
    void onSuccessSubmitAttachment();
    void onFailedSubmitAttachment(String message);
    void onTokenAttachmentExpired();
}
