package re.rezaprelo.presenter.mvpview;

import re.rezaprelo.model.LoginResponse;
import re.rezaprelo.model.LoveListResponse;

/**
 * Created by Reza on 15/12/2017.
 */

public interface LoveListMvpView extends BaseMvpView {
    void onPreLoading();
    void onSuccessLoveList(LoveListResponse loveListResponse);
    void onFailedLoveList(String message);
}
