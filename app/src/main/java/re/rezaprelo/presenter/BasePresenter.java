package re.rezaprelo.presenter;

/**
 * Created by Reza on 14/12/2017.
 */

public interface BasePresenter<V> {
    void attachView(V view);
    void detachView();
}

