package re.rezaprelo.presenter;

import android.util.Log;

import javax.inject.Inject;

import re.rezaprelo.App;
import re.rezaprelo.helper.ErrorRetrofitUtil;
import re.rezaprelo.model.LoginRequest;
import re.rezaprelo.model.LoginResponse;
import re.rezaprelo.model.LoveListResponse;
import re.rezaprelo.presenter.mvpview.LoginMvpView;
import re.rezaprelo.presenter.mvpview.LoveListMvpView;
import re.rezaprelo.services.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Reza on 15/12/2017.
 */

public class LoveListPresenter implements BasePresenter<LoveListMvpView>{


    /**
     * Injection API Service
     */
    @Inject
    ApiService mApiService;

    /**
     * Injection error response from retrofit
     */
    @Inject
    ErrorRetrofitUtil mErrorRetrofitUtil;

    private LoveListMvpView loveListMvpView;
    private Call<LoveListResponse> call;

    public LoveListPresenter() {
        App.appComponent().inject(this);
    }


    @Override
    public void attachView(LoveListMvpView view) {

        loveListMvpView = view;
    }

    @Override
    public void detachView() {
        loveListMvpView = null;
        if (call != null) call.cancel();
    }

    public void callLoveList(String key) {
        loveListMvpView.onPreLoading();
        call = mApiService.loveList(key);
        call.enqueue(new Callback<LoveListResponse>() {
            @Override
            public void onResponse(Call<LoveListResponse> call, Response<LoveListResponse> response) {
                if (response != null) {
                    if (response.code() == 200) {
                        processData(response);
                        loveListMvpView.onSuccessLoveList(response.body());
                    } else {
                        loveListMvpView.onFailedLoveList(response.message());
                    }
                } else {
                    loveListMvpView.onFailedLoveList(mErrorRetrofitUtil.parseError(response)
                            .getMeta().getMessage());
                }
            }

            @Override
            public void onFailure(Call<LoveListResponse> call, Throwable t) {
                loveListMvpView.onFailedLoveList(mErrorRetrofitUtil.responseFailedError(t));
            }
        });
    }

    /**
     * Processing data from retrofit for save in preference
     * @param response response data from retrofit
     */
    private void processData(Response<LoveListResponse>response) {
        Log.d("DATA MODEL", String.valueOf(response));
    }

}

