package re.rezaprelo.model;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.List;

/**
 * Created by Reza on 15/12/2017.
 */
@Parcel
public class Datum {


    @SerializedName("_id")
    public String id;

    @SerializedName("seller_id")
    public String sellerId;

    @SerializedName("name")
    public String name;

    @SerializedName("category_id")
    public String categoryId;

    @SerializedName("brand_id")
    public String brandId;

    @SerializedName("special_story")
    public String specialStory;

    @SerializedName("price")
    public Integer price;

    @SerializedName("price_original")
    public Integer priceOriginal;

    @SerializedName("status")
    public Integer status;

    @SerializedName("free_ongkir")
    public Integer freeOngkir;

    @SerializedName("num_lovelist")
    public Integer numLovelist;

    @SerializedName("num_comment")
    public Integer numComment;

    @SerializedName("permalink")
    public String permalink;

    @SerializedName("display_picts")
    public List<String> displayPicts = null;

    @SerializedName("update_time_uuid")
    public String updateTimeUuid;

    @SerializedName("update_time")
    public String updateTime;

    @SerializedName("proposed_brand")
    public String proposedBrand;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSellerId() {
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getSpecialStory() {
        return specialStory;
    }

    public void setSpecialStory(String specialStory) {
        this.specialStory = specialStory;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getPriceOriginal() {
        return priceOriginal;
    }

    public void setPriceOriginal(Integer priceOriginal) {
        this.priceOriginal = priceOriginal;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getFreeOngkir() {
        return freeOngkir;
    }

    public void setFreeOngkir(Integer freeOngkir) {
        this.freeOngkir = freeOngkir;
    }

    public Integer getNumLovelist() {
        return numLovelist;
    }

    public void setNumLovelist(Integer numLovelist) {
        this.numLovelist = numLovelist;
    }

    public Integer getNumComment() {
        return numComment;
    }

    public void setNumComment(Integer numComment) {
        this.numComment = numComment;
    }

    public String getPermalink() {
        return permalink;
    }

    public void setPermalink(String permalink) {
        this.permalink = permalink;
    }

    public List<String> getDisplayPicts() {
        return displayPicts;
    }

    public void setDisplayPicts(List<String> displayPicts) {
        this.displayPicts = displayPicts;
    }

    public String getUpdateTimeUuid() {
        return updateTimeUuid;
    }

    public void setUpdateTimeUuid(String updateTimeUuid) {
        this.updateTimeUuid = updateTimeUuid;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getProposedBrand() {
        return proposedBrand;
    }

    public void setProposedBrand(String proposedBrand) {
        this.proposedBrand = proposedBrand;
    }
}
