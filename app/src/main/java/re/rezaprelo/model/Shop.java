package re.rezaprelo.model;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by Reza on 14/12/2017.
 */

@Parcel
public class Shop {

    @SerializedName("status")

    public Integer status;
    @SerializedName("start_date")

    public String startDate;
    @SerializedName("end_date")

    public String endDate;


}
