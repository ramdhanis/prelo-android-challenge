package re.rezaprelo.model;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.List;

/**
 * Created by Reza on 15/12/2017.
 */
@Parcel
public class LoveListResponse {


    @SerializedName("_data")
    public List<Datum> data;

    public List<Datum> getDatum ()
    {
        return data;
    }

}
