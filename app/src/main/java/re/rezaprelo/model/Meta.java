package re.rezaprelo.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Reza on 14/12/2017.
 */

public class Meta {
    @SerializedName("message")
    private String message;

    @SerializedName("succeded")
    private boolean succeded;

    @SerializedName("code")
    private String code;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSucceded() {
        return succeded;
    }

    public void setSucceded(boolean succeded) {
        this.succeded = succeded;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
