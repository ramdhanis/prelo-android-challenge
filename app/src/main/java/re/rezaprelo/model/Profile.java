package re.rezaprelo.model;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by Reza on 14/12/2017.
 */

@Parcel
public class Profile {

    @SerializedName("pict")

    public String pict;
    @SerializedName("gender")

    public String gender;
    @SerializedName("phone")

    public String phone;
    @SerializedName("province_id")

    public String provinceId;
    @SerializedName("region_id")

    public String regionId;
    @SerializedName("subdistrict_id")

    public String subdistrictId;
    @SerializedName("subdistrict_name")

    public String subdistrictName;

}
