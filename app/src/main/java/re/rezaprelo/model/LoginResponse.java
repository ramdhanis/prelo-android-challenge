package re.rezaprelo.model;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by Reza on 14/12/2017.
 */
public class LoginResponse {

    @SerializedName("_data")
    public Data data;

}
