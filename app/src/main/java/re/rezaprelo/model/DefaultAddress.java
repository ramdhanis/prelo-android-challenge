package re.rezaprelo.model;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by Reza on 14/12/2017.
 */

@Parcel
public class DefaultAddress  {

    @SerializedName("_id")
    public String id;
    @SerializedName("is_default")

    public Boolean isDefault;
    @SerializedName("create_time")

    public String createTime;
    @SerializedName("postal_code")

    public String postalCode;
    @SerializedName("address")

    public String address;
    @SerializedName("subdistrict_name")

    public String subdistrictName;
    @SerializedName("subdistrict_id")

    public String subdistrictId;
    @SerializedName("region_name")

    public String regionName;
    @SerializedName("region_id")

    public String regionId;
    @SerializedName("province_name")

    public String provinceName;
    @SerializedName("province_id")

    public String provinceId;
    @SerializedName("phone")

    public String phone;
    @SerializedName("owner_name")

    public String ownerName;
    @SerializedName("address_name")

    public String addressName;

}
