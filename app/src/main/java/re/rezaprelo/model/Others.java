package re.rezaprelo.model;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.List;

/**
 * Created by Reza on 14/12/2017.
 */

@Parcel
public class Others {


    @SerializedName("register_time")

    public String registerTime;
    @SerializedName("platform_register_from")

    public String platformRegisterFrom;
    @SerializedName("last_login")

    public String lastLogin;
    @SerializedName("register_by_fb")

    public Boolean registerByFb;
    @SerializedName("register_by_path")

    public Boolean registerByPath;
    @SerializedName("register_by_twitter")

    public Boolean registerByTwitter;
    @SerializedName("register_by_google")

    public Boolean registerByGoogle;
    @SerializedName("last_seen")

    public String lastSeen;
    @SerializedName("last_sent_sms")

    public String lastSentSms;
    @SerializedName("is_phone_verified")

    public Boolean isPhoneVerified;
    @SerializedName("phone_code")

    public String phoneCode;
    @SerializedName("total_sent_sms")

    public Integer totalSentSms;
    @SerializedName("device_registration_id")

    public String deviceRegistrationId;
    @SerializedName("device_type")

    public String deviceType;
    @SerializedName("email_code")

    public String emailCode;
    @SerializedName("is_email_verified")

    public Boolean isEmailVerified;
    @SerializedName("num_reviewer")

    public Integer numReviewer;
    @SerializedName("total_star")

    public Integer totalStar;
    @SerializedName("max_push_in_period")

    public Integer maxPushInPeriod;
    @SerializedName("push_product_period")

    public Integer pushProductPeriod;
    @SerializedName("category_preferences_ids")

    public List<String> categoryPreferencesIds = null;
    @SerializedName("fa_id")

    public List<String> faId = null;
    @SerializedName("ga_id")

    public List<String> gaId = null;


}
