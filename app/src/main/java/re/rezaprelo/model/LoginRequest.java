package re.rezaprelo.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Reza on 14/12/2017.
 */

public class LoginRequest {

    @SerializedName("username_or_email")
    private String usernameoremail;


    @SerializedName("password")
    private String password;

    public LoginRequest(String usernameoremail, String password) {
        this.usernameoremail = usernameoremail;
        this.password = password;
    }
}
