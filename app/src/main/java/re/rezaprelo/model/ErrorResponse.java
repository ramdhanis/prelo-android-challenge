package re.rezaprelo.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Reza on 14/12/2017.
 */

public class ErrorResponse {
    @SerializedName("meta")
    private Meta meta;

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }
}
