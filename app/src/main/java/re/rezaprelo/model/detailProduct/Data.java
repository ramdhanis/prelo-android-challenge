package re.rezaprelo.model.detailProduct;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Reza on 27/12/2017.
 */

public class Data {


    @SerializedName("_id")
    public String id;
    @SerializedName("name")
    public String name;
    public String categoryId;
    @SerializedName("price")
    public Integer price;
    @SerializedName("price_original")
    public Integer priceOriginal;
    @SerializedName("display_picts")
    public List<String> displayPicts = null;
    @SerializedName("seller")
    public Seller seller;
}
