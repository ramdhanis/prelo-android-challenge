package re.rezaprelo.model.detailProduct;

import com.google.gson.annotations.SerializedName;



/**
 * Created by Reza on 27/12/2017.
 */

public class ProductDetailResponse {

    @SerializedName("_data")

    public Data data;
}
