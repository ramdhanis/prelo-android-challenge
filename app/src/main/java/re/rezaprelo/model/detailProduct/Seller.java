package re.rezaprelo.model.detailProduct;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Reza on 27/12/2017.
 */

public class Seller {


    @SerializedName("_id")
    public String id;
    @SerializedName("email")
    public String email;
    @SerializedName("username")
    public String username;
    @SerializedName("fullname")
    public String fullname;
    @SerializedName("pict")
    public String pict;
    }
