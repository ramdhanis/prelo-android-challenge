package re.rezaprelo.model;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.List;

/**
 * Created by Reza on 14/12/2017.
 */
@Parcel
public class Data {

    @SerializedName("shipping_preferences_ids")
//
    public List<String> shippingPreferencesIds = null;
    @SerializedName("_id")

    public String id;
    @SerializedName("profile")

    public Profile profile;
    @SerializedName("username")

    public String username;
    @SerializedName("email")

    public String email;
    @SerializedName("fullname")

    public String fullname;
    @SerializedName("others")

    public Others others;
    @SerializedName("main_bank_account")

    public MainBankAccount mainBankAccount;
    @SerializedName("rent")

    public Rent rent;
    @SerializedName("shop")

    public Shop shop;
    @SerializedName("default_address")

    public DefaultAddress defaultAddress;
    @SerializedName("token")

    public String token;

}
