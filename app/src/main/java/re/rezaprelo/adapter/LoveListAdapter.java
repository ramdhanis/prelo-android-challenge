package re.rezaprelo.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import re.rezaprelo.R;
import re.rezaprelo.model.Datum;
import re.rezaprelo.model.LoveListResponse;

/**
 * Created by Reza on 15/12/2017.
 */

public class LoveListAdapter extends RecyclerView.Adapter<LoveListAdapter.LoveListHolder> implements View.OnClickListener {

    private Listener mListener;
    private Context mContext;
    public LoveListResponse mData;
    public List<Datum> mDatum;



    public LoveListAdapter(List<Datum> datum, Listener listener) {
        this.mDatum = datum;
        mListener = listener;
    }

    public LoveListResponse getData() {
        return mData;
    }

    public void setData(List<Datum> list) {
        this.mDatum = list;
    }

    @Override
    public LoveListAdapter.LoveListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View contactView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_love_list, parent, false);
        mContext = parent.getContext();
        return new LoveListHolder(contactView);
    }

    @Override
    public void onBindViewHolder(LoveListHolder holder, final int position) {
        holder.bind(position, mDatum);
        holder.setIsRecyclable(false);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onItemClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDatum.size();
    }

    @Override
    public void onClick(View view) {


    }


    public class LoveListHolder extends RecyclerView.ViewHolder {

        Datum dataLoveList;

        public TextView tvNamaBro;
        public TextView tvLoveList;
        public ImageView ivGambarBro;

        public LoveListHolder(final View itemView) {

            super(itemView);
            tvNamaBro = itemView.findViewById(R.id.tv_example_two);
            tvLoveList = itemView.findViewById(R.id.tv_love_list);
            ivGambarBro = itemView.findViewById(R.id.iv_example_one);
        }

        public void bind(int position, List<Datum> data) {

            dataLoveList = data.get(position);

//            final String productId = dataLoveList.id;

            tvNamaBro.setText(dataLoveList.getName());
            tvLoveList.setText("Rp. " + dataLoveList.getPriceOriginal().toString());
            Glide.with(mContext)
                    .load(dataLoveList.displayPicts.get(0))
                    .into(ivGambarBro);

//            itemView.setOnClickListener(new View.OnClickListener() {
//                                            @Override
//                                            public void onClick(View view) {
//
//                                                Intent intent = new Intent(mContext, DetailProductActivity.class);
//                                                intent.putExtra("ProductId", productId);
//                                                mContext.startActivity(intent);
//
//                                            }
//                                        }
//
//            );

        }
    }

    public interface Listener{
        void onItemClick(int position);
    }
}

