package re.rezaprelo.helper;

import java.io.IOException;
import java.lang.annotation.Annotation;

import javax.inject.Inject;

import okhttp3.ResponseBody;
import re.rezaprelo.App;
import re.rezaprelo.model.ErrorResponse;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Reza on 14/12/2017.
 */

public class ErrorRetrofitUtil {

    @Inject
    Retrofit retrofit;

    public ErrorRetrofitUtil() {
        App.appComponent().inject(this);
    }

    public ErrorResponse parseError(Response<?> response) {
        Converter<ResponseBody, ErrorResponse> converter = retrofit.
                responseBodyConverter(ErrorResponse.class, new Annotation[0]);
        ErrorResponse errorResponse = null;
        try {
            errorResponse = converter.convert(response.errorBody());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return errorResponse;
    }

    public String responseFailedError(Throwable e) {
        if (e != null) {
            if (e instanceof IOException) {
                return "Tidak ada koneksi internet";
            }
        }
        return "Unknown error";
    }
}
