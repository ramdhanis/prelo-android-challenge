package re.rezaprelo.services;

import re.rezaprelo.model.LoginRequest;
import re.rezaprelo.model.LoginResponse;
import re.rezaprelo.model.LoveListResponse;
import re.rezaprelo.model.detailProduct.ProductDetailResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Reza on 14/12/2017.
 */

public interface ApiService {


    @POST("auth/login")
    Call<LoginResponse> login(@Body LoginRequest loginRequest);

    @GET("me/lovelist")
    Call<LoveListResponse> loveList(@Header("Authorization") String key);

    @GET("product/{product_id}")
    Call<ProductDetailResponse> productDetail(@Path("product_id") String productId);

}
