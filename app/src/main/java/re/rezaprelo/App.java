package re.rezaprelo;

import android.app.Application;

import re.rezaprelo.dagger.ApplicationComponent;
import re.rezaprelo.dagger.ApplicationModule;
import re.rezaprelo.dagger.DaggerApplicationComponent;

/**
 * Created by Reza on 14/12/2017.
 */

public class App extends Application {

    private static ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        applicationComponent = DaggerApplicationComponent.builder().applicationModule(new ApplicationModule(this)).build();
        applicationComponent.inject(this);


    }

    public static ApplicationComponent appComponent() {
        return applicationComponent;
    }

}

