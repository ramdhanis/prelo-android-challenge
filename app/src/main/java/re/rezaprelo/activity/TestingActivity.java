package re.rezaprelo.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import re.rezaprelo.R;

/**
 * Created by Reza on 07/01/2018.
 */

public class TestingActivity extends AppCompatActivity implements View.OnClickListener{

    @BindView(R.id.et_input_payment)
    EditText etInputPayment;
    @BindView(R.id.tv_one)
    TextView tvPriceOne;
    @BindView(R.id.tv_two)
    TextView tvPriceTwo;
    @BindView(R.id.btn_confirm_payment)
    Button btnConfrimPayment;
    @BindView(R.id.iv_triangle)
    ImageView ivTriangle;
    @BindView(R.id.ll_description)
    LinearLayout llDescription;

    String dataString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_layout);
        ButterKnife.bind(this);
        btnConfrimPayment.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if (view == btnConfrimPayment) {
            initData();
        }
    }


    private void initData() {
        if (getInputPayment().length() < 4) {
            tvPriceOne.setText(getInputPayment());
            tvPriceTwo.setVisibility(View.GONE);
            ivTriangle.setVisibility(View.GONE);
            llDescription.setVisibility(View.GONE);
        } else {
            dataString = getInputPayment();
//            String tryString ="\\.";

//            String[] parts = dataString.split(String.valueOf(dataString.lastIndexOf(substr)));
//            String before = parts[0];
//            String after = parts[1];
//            String str = "123.456";
//
//            final int position = dataString.lastIndexOf(tryString);
//                //if the substring does occur within the string, set the values accordingly
//                String before = dataString.substring(0, position);
//                String after = dataString.substring(position + tryString.length());
//
//                tvPriceOne.setText(before);
//                tvPriceTwo.setText(after);
            String before = dataString.substring(0, dataString.length() -3);
            String after = dataString.substring(dataString.length() - 3);

//
//            String[] parts = dataString.split("\\.");
//            String before = parts[0];
//            String after = parts[1];

//            String before = dataString.substring(0,dataString.lastIndexOf(substr));
//            String after = dataString.substring(dataString.indexOf(substr) + substr.length());
//
            tvPriceOne.setText(before);
            tvPriceTwo.setText(after);
            tvPriceTwo.setVisibility(View.VISIBLE);
            ivTriangle.setVisibility(View.VISIBLE);
            llDescription.setVisibility(View.VISIBLE);
//            int last = dataString.lastIndexOf("\\.");
//            String [] split = dataString.lastIndexOf("\\.");
        }

    }


    protected String getInputPayment() {
        return etInputPayment.getText().toString();
    }


}
