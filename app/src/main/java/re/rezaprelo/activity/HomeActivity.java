package re.rezaprelo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import re.rezaprelo.App;
import re.rezaprelo.R;
import re.rezaprelo.adapter.LoveListAdapter;
import re.rezaprelo.model.Data;
import re.rezaprelo.model.Datum;
import re.rezaprelo.model.LoginRequest;
import re.rezaprelo.model.LoveListResponse;
import re.rezaprelo.presenter.LoginPresenter;
import re.rezaprelo.presenter.LoveListPresenter;
import re.rezaprelo.presenter.mvpview.LoginMvpView;
import re.rezaprelo.presenter.mvpview.LoveListMvpView;

/**
 * Created by Reza on 14/12/2017.
 */

public class HomeActivity extends AppCompatActivity implements LoveListMvpView, LoveListAdapter.Listener {

    @Inject
    LoveListPresenter loveListPresenter;
    @BindView(R.id.tv_full_name)
    TextView tvFullName;
    @BindView(R.id.iv_profile)
    CircleImageView ivProfile;
    @BindView(R.id.tv_user_name)
    TextView tvUserName;
    @BindView(R.id.tv_email)
    TextView tvEmail;
    @BindView(R.id.tv_alamat)
    TextView tvAlamat;
    @BindView(R.id.pb_love_list)
    ProgressBar progressBar;
    @BindView(R.id.rv_love_list)
    RecyclerView recyclerViewLoveList;


    private LoveListResponse loveListResponseActivity;
    private List<Datum> mList = new ArrayList<>();
    private LoveListAdapter adapter;
    private String tokenLogin;
    private Data datalogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        App.appComponent().inject(this);
        loveListPresenter.attachView(this);
        ButterKnife.bind(this);
        Bundle bundle = getIntent().getExtras();
        datalogin = Parcels.unwrap(bundle.getParcelable("datalogin"));
        tokenLogin = "Token " + datalogin.token;
        initData();
        callLoveList();
    }


    public void initData() {

        Glide.with(this).load(datalogin.profile.pict).into(ivProfile);
        tvFullName.setText(datalogin.fullname);
        tvUserName.setText(datalogin.username);
        tvEmail.setText(datalogin.email);
        tvAlamat.setText(datalogin.defaultAddress.subdistrictName + ", " + datalogin.defaultAddress.regionName + ", " + datalogin.defaultAddress.provinceName);


    }

    public void callLoveList() {
        loveListPresenter.callLoveList(tokenLogin);
    }

    public void initRecycleLoveList() {


        adapter = new LoveListAdapter(mList, this);

        recyclerViewLoveList.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewLoveList.setAdapter(adapter);


    }

    @Override
    public void onPreLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onSuccessLoveList(LoveListResponse loveListResponse) {


        progressBar.setVisibility(View.GONE);
        loveListResponseActivity = loveListResponse;
        mList = loveListResponse.getDatum();
        initRecycleLoveList();

    }

    @Override
    public void onFailedLoveList(String message) {

        progressBar.setVisibility(View.GONE);
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemClick(int position) {


        Intent intent = new Intent(this, DetailProductActivity.class);
        intent.putExtra("ProductId", mList.get(position).getId());
        this.startActivity(intent);
    }
}
