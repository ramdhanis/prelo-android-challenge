package re.rezaprelo.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;


import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import re.rezaprelo.App;
import re.rezaprelo.R;
import re.rezaprelo.model.detailProduct.ProductDetailResponse;
import re.rezaprelo.presenter.ProductDetailPresenter;
import re.rezaprelo.presenter.mvpview.DetailProductMvpView;

/**
 * Created by Reza on 27/12/2017.
 */

public class DetailProductActivity extends AppCompatActivity implements DetailProductMvpView {

    @Inject
    ProductDetailPresenter productDetailPresenter;
    @BindView(R.id.iv_main_picture)
    ImageView ivMainPicture;
    @BindView(R.id.iv_user_profile)
    CircleImageView ivProfile;
    @BindView(R.id.tv_main_title)
    TextView tvMainTitle;
    @BindView(R.id.tv_price)
    TextView tvPrice;
    @BindView(R.id.tv_name_title)
    TextView tvUserName;
    @BindView(R.id.tv_email_user)
    TextView tvEmailUser;
    @BindView(R.id.pb_detail_product)
    ProgressBar progressBar;


    private ProductDetailResponse productDetailsResponse;
    private String detailProductId, priceProduct;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_product);

        ButterKnife.bind(this);
        App.appComponent().inject(this);
        productDetailPresenter.attachView(this);
        detailProductId = getIntent().getStringExtra("ProductId");
        callProductDetail(detailProductId);
    }


    public void initData() {

        priceProduct = productDetailsResponse.data.price.toString();

        Glide.with(this).load(productDetailsResponse.data.displayPicts.get(0)).into(ivMainPicture);
        Glide.with(this).load(productDetailsResponse.data.seller.pict).into(ivProfile);
        tvMainTitle.setText(productDetailsResponse.data.name);
        tvPrice.setText("Rp. " + priceProduct);
        tvUserName.setText(productDetailsResponse.data.seller.fullname);
        tvEmailUser.setText(productDetailsResponse.data.seller.email);



    }

    public void callProductDetail(String productId) {
        productDetailPresenter.callProductDetail(productId);
    }


    @Override
    public void onPreLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onSuccessProductDetail(ProductDetailResponse productDetailResponse) {
        productDetailsResponse = productDetailResponse;
        progressBar.setVisibility(View.GONE);
        initData();

    }

    @Override
    public void onFailedProductDetail(String message) {

        progressBar.setVisibility(View.GONE);
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

}

