package re.rezaprelo.activity;

import android.content.Intent;
import android.os.Parcel;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.parceler.Parcels;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import re.rezaprelo.App;
import re.rezaprelo.R;
import re.rezaprelo.helper.Constant;
import re.rezaprelo.model.LoginRequest;
import re.rezaprelo.model.LoginResponse;
import re.rezaprelo.presenter.LoginPresenter;
import re.rezaprelo.presenter.mvpview.LoginMvpView;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, LoginMvpView {


    @Inject
    LoginPresenter loginPresenter;
    @BindView(R.id.progres_bar)
    ProgressBar progressBar;
    @BindView(R.id.et_username)
    EditText etUsername;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.btn_login)
    Button btnLogin;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        btnLogin.setOnClickListener(this);
        App.appComponent().inject(this);
        loginPresenter.attachView(this);
    }



    @Override
    public void onClick(View view) {
        if (view == btnLogin) {
            loginWithEmail();
        }
    }

    private void loginWithEmail() {
        Boolean flag = true;
        if (getUsername().length() < 6) {
            etUsername.setError(getResources().getString(R.string.label_error_username));
            flag = false;
        }
        if (getPassword().length() < 6) {
            etPassword.setError(getResources().getString(R.string.label_error_password));
            flag = false;
        }
        if (getUsername().length() < 6 && getPassword().length() < 6) {
            etPassword.setError(getResources().getString(R.string.label_error_password));
            flag = false;
        }
        if (flag) {
            callLogin();
        }
    }

    public void callLogin() {
        loginPresenter.callLogin(new LoginRequest(getUsername(), getPassword()));
    }


    protected String getUsername() {
        return etUsername.getText().toString();
    }

    protected String getPassword() {
        return etPassword.getText().toString();
    }

    @Override
    public void onPreLogin() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onSuccessLogin(LoginResponse loginResponse) {

        progressBar.setVisibility(View.GONE);
        Bundle bundle = new Bundle();
        bundle.putParcelable("datalogin", Parcels.wrap(loginResponse.data));
        Intent intent = new Intent(this, HomeActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
        finish();

    }

    @Override
    public void onFailedLogin(String message) {

        progressBar.setVisibility(View.GONE);
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
