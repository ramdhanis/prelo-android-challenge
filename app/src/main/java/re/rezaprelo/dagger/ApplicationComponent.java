package re.rezaprelo.dagger;

import dagger.Component;
import re.rezaprelo.App;
import re.rezaprelo.activity.DetailProductActivity;
import re.rezaprelo.activity.HomeActivity;
import re.rezaprelo.activity.LoginActivity;
import re.rezaprelo.helper.ErrorRetrofitUtil;
import re.rezaprelo.presenter.LoginPresenter;
import re.rezaprelo.presenter.LoveListPresenter;
import re.rezaprelo.presenter.ProductDetailPresenter;

/**
 * Created by Reza on 14/12/2017.
 */

@ApplicationScope
@Component(modules = {ApplicationModule.class})
public interface ApplicationComponent {
    void inject(App app);
//    void inject(HomeFragment homeFragment);
    void inject(ErrorRetrofitUtil errorRetrofitUtil);
    void inject(LoginPresenter loginPresenter);
    void inject(LoveListPresenter loveListPresenter);
    void inject(ProductDetailPresenter productDetailPresenter);
    void inject(DetailProductActivity detailProductActivity);
    void inject(LoginActivity loginActivity);
    void inject(HomeActivity homeActivity);

}
