package re.rezaprelo.dagger;

import android.app.Application;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import dagger.Module;
import dagger.Provides;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import re.rezaprelo.App;
import re.rezaprelo.helper.ErrorRetrofitUtil;
import re.rezaprelo.presenter.LoginPresenter;
import re.rezaprelo.presenter.LoveListPresenter;
import re.rezaprelo.presenter.ProductDetailPresenter;
import re.rezaprelo.services.ApiService;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Reza on 14/12/2017.
 */
@Module
public class ApplicationModule  {
    private final App mApp;

    public ApplicationModule(App app) {
        mApp = app;
    }

    @Provides
    @ApplicationScope
    Application provideApplication() {
        return mApp;
    }

    @Provides
    @ApplicationScope
    HttpUrl provideHttpUrl(Application app) {
        return HttpUrl.parse("https://dev.prelo.id/api/");
    }

    @Provides
    @ApplicationScope
    Retrofit provideRetrofit(HttpUrl httpUrl, OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(httpUrl)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @Provides
    @ApplicationScope
    ApiService provideApiService(Retrofit retrofit) {
        return retrofit.create(ApiService.class);
    }

    @Provides
    @ApplicationScope
    OkHttpClient provideOkHttpClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .readTimeout(3, TimeUnit.MINUTES)
                .writeTimeout(3, TimeUnit.MINUTES)
                .build();
        return client;
    }

    @Provides
    @ApplicationScope
    Gson provideGson() {
        Gson gson = new GsonBuilder()
                .create();
        return gson;
    }

    @Provides
    @ApplicationScope
    ErrorRetrofitUtil provideErrorUtil() {
        return new ErrorRetrofitUtil();
    }

    @Provides
    @ApplicationScope
    LoginPresenter provideLoginPresenter() {
        return new LoginPresenter();
    }

    @Provides
    @ApplicationScope
    LoveListPresenter provideLoveListPresenter() {
        return new LoveListPresenter();
    }

    @Provides
    @ApplicationScope
    ProductDetailPresenter productDetailPresenter() {
        return new ProductDetailPresenter();
    }
}

